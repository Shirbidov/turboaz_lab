package az.ingress.turboaz.car_market;

import az.ingress.turboaz.car_market.domain.ModelEntity;
import az.ingress.turboaz.car_market.domain.UserRoleEntity;
import az.ingress.turboaz.car_market.repository.UserRoleRepository;
import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

@SpringBootApplication
@RequiredArgsConstructor
public class CarMarketApplication implements CommandLineRunner {

//	private final EntityManagerFactory entityManagerFactory;

	private final UserRoleRepository userRoleRepository;


	public static void main(String[] args) {
		SpringApplication.run(CarMarketApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

//	final EntityManager entityManager = entityManagerFactory.createEntityManager();
//	final EntityTransaction transaction = entityManager.getTransaction();
//	transaction.begin();
//
//		//Entity in the new state
//		ModelEntity model =
//				ModelEntity
//						.builder()
//						.name("Jeep")
//						.build();
//
//		// Entity in the manage mode
//		entityManager.persist(model);
//		System.out.println("1 Entity in the manage mode "+entityManager.contains(model));
//
//		// Entity in detach mode
//		entityManager.detach(model);
//		System.out.println("2 Entity in the detach mode "+entityManager.contains(model));
//
//
//		// Entity in the manage mode - ozunun yaratdigi instanci yaradir
//		ModelEntity mergeModelEntity = entityManager.merge(model);
//		System.out.println("3 Entity in the manage mode "+entityManager.contains(mergeModelEntity));
//
//
//		// Entity in detach mode
//		entityManager.clear();
//		System.out.println("4 Entity in the detach mode "+ entityManager.contains(model));
//
//		// Entity in the manage mode
//		entityManager.find(ModelEntity.class, model.getId());
//		System.out.println("5 Entity in the manage mode "+ entityManager.contains(model));
//
//
//		entityManager.close();
//		transaction.commit();
////		System.out.println("Exeption "+);

		UserRoleEntity userRoleEntity
				= UserRoleEntity
				.builder()
				.username("user")
				.password("{noop}1234")
//				.authorities(List.of())
				.accountNonExpired(true)
				.accountNonLocked(true)
				.enabled(true)
				.credentialsNonExpired(true)
				.build();
//		userRoleRepository.save(userRoleEntity);


	}
}
