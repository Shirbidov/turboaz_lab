package az.ingress.turboaz.car_market.service.service;

import az.ingress.turboaz.car_market.dto.request.ManufactureRequestDto;
import az.ingress.turboaz.car_market.dto.request.ModelRequestDto;
import az.ingress.turboaz.car_market.dto.response.ManufactureResponseDto;
import az.ingress.turboaz.car_market.dto.response.ModelResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ModelService {


    ModelResponseDto createModel(ModelRequestDto modelRequestDto);

    ModelResponseDto getModelById(Long id);

    ModelResponseDto updateModel(Long id, ModelRequestDto modelRequestDto);

    void deleteModelById(Long id);

    Page<ModelResponseDto> modelList(Pageable page);

    ModelResponseDto insertModelToManufactory(String modelName,String manufactoryName);



    //todo delete all model


}
