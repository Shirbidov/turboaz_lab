package az.ingress.turboaz.car_market.service.Impl;

import az.ingress.turboaz.car_market.domain.ProductDetailEntity;
import az.ingress.turboaz.car_market.domain.ProductEntity;
import az.ingress.turboaz.car_market.dto.request.ProductDetailRequestDto;
import az.ingress.turboaz.car_market.dto.request.ProductRequestDto;
import az.ingress.turboaz.car_market.dto.response.ProductDetailResponseDto;
import az.ingress.turboaz.car_market.dto.response.ProductResponseDto;
import az.ingress.turboaz.car_market.repository.ProductDetailRepository;
import az.ingress.turboaz.car_market.repository.ProductRepository;
import az.ingress.turboaz.car_market.service.service.ProductDetailService;
import az.ingress.turboaz.car_market.service.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductDetailServiceImpl implements ProductDetailService {

    private final ProductDetailRepository productDetailRepository;


    @Override
    public ProductDetailResponseDto createProductDetail(ProductDetailRequestDto productDetailRequestDto) {
//todo add here if required value is null to give exception about all field

        ProductDetailEntity productDetailEntity =
                ProductDetailEntity
                        .builder()
                        .city(productDetailRequestDto.getCity())
                        .color(productDetailRequestDto.getColor())
                        .isNew(productDetailRequestDto.getIsNew())
//                        .model(productDetailRequestDto.getModel())
                        .driveUnit(productDetailRequestDto.getDriveUnit())
                        .engine(productDetailRequestDto.getEngine())
                        .mileage(productDetailRequestDto.getMileage())
//                        .manufacturer(productDetailRequestDto.getManufacturer())
                        .manufacturerDate(productDetailRequestDto.getManufacturerDate())
                        .numberOfSeats(productDetailRequestDto.getNumberOfSeats())
                        .transmissionType(productDetailRequestDto.getTransmissionType())
                        .driveUnit(productDetailRequestDto.getDriveUnit())
                        .build();
        final ProductDetailEntity productDetailEntitySaved = productDetailRepository.save(productDetailEntity);
        return ProductDetailResponseDto
                .builder()
                .id(productDetailEntitySaved.getId())
                .city(productDetailEntitySaved.getCity())
                .color(productDetailEntitySaved.getColor())
                .isNew(productDetailEntitySaved.getIsNew())
//                .model(productDetailEntitySaved.getModel())
                .driveUnit(productDetailEntitySaved.getDriveUnit())
                .engine(productDetailEntitySaved.getEngine())
                .mileage(productDetailEntitySaved.getMileage())
//                .manufacturer(productDetailEntitySaved.getManufacturer())
                .manufacturerDate(productDetailEntitySaved.getManufacturerDate())
                .numberOfSeats(productDetailEntitySaved.getNumberOfSeats())
                .transmissionType(productDetailEntitySaved.getTransmissionType())
                .driveUnit(productDetailEntitySaved.getDriveUnit())
                .build();

    }

    @Override
    public ProductDetailResponseDto getProductDetailById(Long id) {
        final ProductDetailEntity productDetail = productDetailRepository.findById(id)
                .orElseThrow(); // todo add here exception not found and id doesn not get letters or characters
        return ProductDetailResponseDto
                //todo change builder with mapper
                .builder()
                .id(productDetail.getId())
                .city(productDetail.getCity())
                .color(productDetail.getColor())
                .isNew(productDetail.getIsNew())
//                .model(productDetail.getModel())
                .driveUnit(productDetail.getDriveUnit())
                .engine(productDetail.getEngine())
                .mileage(productDetail.getMileage())
//                .manufacturer(productDetail.getManufacturer())
                .manufacturerDate(productDetail.getManufacturerDate())
                .numberOfSeats(productDetail.getNumberOfSeats())
                .transmissionType(productDetail.getTransmissionType())
                .driveUnit(productDetail.getDriveUnit())
                .build();
    }

    @Override
    public ProductDetailResponseDto updateProductDetail(Long id, ProductDetailRequestDto productDetailRequestDto) {
        ProductDetailEntity productDetailEntity =
                ProductDetailEntity
                        .builder()
                        .id(id)
                        .city(productDetailRequestDto.getCity())
                        .color(productDetailRequestDto.getColor())
                        .isNew(productDetailRequestDto.getIsNew())
//                        .model(productDetailRequestDto.getModel())
                        .driveUnit(productDetailRequestDto.getDriveUnit())
                        .engine(productDetailRequestDto.getEngine())
                        .mileage(productDetailRequestDto.getMileage())
//                        .manufacturer(productDetailRequestDto.getManufacturer())
                        .manufacturerDate(productDetailRequestDto.getManufacturerDate())
                        .numberOfSeats(productDetailRequestDto.getNumberOfSeats())
                        .transmissionType(productDetailRequestDto.getTransmissionType())
                        .driveUnit(productDetailRequestDto.getDriveUnit())
                        .build();
        final ProductDetailEntity productDetailSaved = productDetailRepository.save(productDetailEntity);
        return ProductDetailResponseDto
                .builder()
                .id(productDetailSaved.getId())
                .city(productDetailSaved.getCity())
                .color(productDetailSaved.getColor())
                .isNew(productDetailSaved.getIsNew())
//                .model(productDetailSaved.getModel())
                .driveUnit(productDetailSaved.getDriveUnit())
                .engine(productDetailSaved.getEngine())
                .mileage(productDetailSaved.getMileage())
//                .manufacturer(productDetailSaved.getManufacturer())
                .manufacturerDate(productDetailSaved.getManufacturerDate())
                .numberOfSeats(productDetailSaved.getNumberOfSeats())
                .transmissionType(productDetailSaved.getTransmissionType())
                .driveUnit(productDetailSaved.getDriveUnit())
                .build();
    }

    @Override
    public void deleteProductDetailById(Long id) {
        productDetailRepository.deleteById(id);
    }

    @Override
    public Page<ProductDetailResponseDto> productDetailList(@PageableDefault(size = 7, page = 1) Pageable pageable) {
        return productDetailRepository.findAll(pageable)
                .map(productDetailEntity -> ProductDetailResponseDto
                        .builder()
                        .id(productDetailEntity.getId())
                        .city(productDetailEntity.getCity())
                        .color(productDetailEntity.getColor())
                        .isNew(productDetailEntity.getIsNew())
//                        .model(productDetailEntity.getModel())
                        .driveUnit(productDetailEntity.getDriveUnit())
                        .engine(productDetailEntity.getEngine())
                        .mileage(productDetailEntity.getMileage())
//                        .manufacturer(productDetailEntity.getManufacturer())
                        .manufacturerDate(productDetailEntity.getManufacturerDate())
                        .numberOfSeats(productDetailEntity.getNumberOfSeats())
                        .transmissionType(productDetailEntity.getTransmissionType())
                        .driveUnit(productDetailEntity.getDriveUnit())
                        .build()
                );
    }
}
