package az.ingress.turboaz.car_market.service.Impl;

import az.ingress.turboaz.car_market.domain.ManufacturerEntity;
import az.ingress.turboaz.car_market.domain.ModelEntity;
import az.ingress.turboaz.car_market.dto.request.ManufactureRequestDto;
import az.ingress.turboaz.car_market.dto.request.ModelRequestDto;
import az.ingress.turboaz.car_market.dto.response.ManufactureResponseDto;
import az.ingress.turboaz.car_market.dto.response.ModelResponseDto;
import az.ingress.turboaz.car_market.repository.ManufactureRepository;
import az.ingress.turboaz.car_market.repository.ModelRepository;
import az.ingress.turboaz.car_market.service.service.ManufactureService;
import az.ingress.turboaz.car_market.service.service.ModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

@Service
@RequiredArgsConstructor
public class ModelServiceImpl implements ModelService {

    private final ModelRepository modelRepository;
    private final ManufactureRepository manufactureRepository;


    @Override
    public ModelResponseDto createModel(ModelRequestDto modelRequestDto) {
//todo add here if required value is null to give exception about all field

        ModelEntity modelEntity =
                ModelEntity
                        .builder()
                        .name(modelRequestDto.getName())
                        .build();
        final ModelEntity modelEntitySaved = modelRepository.save(modelEntity);
        return ModelResponseDto
                .builder()
                .name(modelEntitySaved.getName())
                .build();
    }


    @Override
    public ModelResponseDto getModelById(Long id) {
        final ModelEntity modelEntity = modelRepository.findById(id)
                .orElseThrow(); // todo add here exception not found and id doesn not get letters or characters
        return ModelResponseDto
                //todo change builder with mapper
                .builder()
                .name(modelEntity.getName())
                .build();
    }

    @Override
    public ModelResponseDto updateModel(Long id, ModelRequestDto modelRequestDto) {

        ModelEntity modelEntity =
                ModelEntity
                        .builder()
                        .id(id)
                        .name(modelRequestDto.getName())
                        .build();
        final ModelEntity modelEntitySaved = modelRepository.save(modelEntity);
        return ModelResponseDto
                .builder()
                .name(modelEntitySaved.getName())
                .build();
    }

    @Override
    public void deleteModelById(Long id) {
        modelRepository.deleteById(id);
    }

    @Override
    public Page<ModelResponseDto> modelList(@PageableDefault(size = 3, page = 1) Pageable pageable) {
        return modelRepository.findAll(pageable)
                .map(modelEntity -> ModelResponseDto
                        .builder()
                        .name(modelEntity.getName())
                        .build()
                );
    }

    @Override
    public ModelResponseDto insertModelToManufactory(String modelName, String manufactoryName) {
        return null;
    }

//    @Override
//    public ModelResponseDto insertModelToManufactory(String modelName, String manufactoryName) {
//        Long manufactoryID = manufactureRepository.findByName(manufactoryName).getId();
//        ModelEntity modelEntity =
//        ModelEntity
//                .builder()
//                .name(modelName)
//                .build();
//        final ModelEntity modelSaved = modelRepository.save(modelEntity);
//
//    return null;
//
//    }
}
