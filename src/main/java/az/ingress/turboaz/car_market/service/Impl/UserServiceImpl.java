package az.ingress.turboaz.car_market.service.Impl;

import az.ingress.turboaz.car_market.domain.ProductEntity;
import az.ingress.turboaz.car_market.domain.UserEntity;
import az.ingress.turboaz.car_market.dto.request.ProductRequestDto;
import az.ingress.turboaz.car_market.dto.request.UserRequestDto;
import az.ingress.turboaz.car_market.dto.response.ProductResponseDto;
import az.ingress.turboaz.car_market.dto.response.UserResponseDto;
import az.ingress.turboaz.car_market.repository.ProductRepository;
import az.ingress.turboaz.car_market.repository.UserRepository;
import az.ingress.turboaz.car_market.service.service.ProductService;
import az.ingress.turboaz.car_market.service.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;


    ////todo add here if required value is null to give exception about all field
    @Override
    public UserResponseDto createUser(UserRequestDto userRequestDto) {
        UserEntity userEntity =
                UserEntity
                        .builder()
                        .name(userRequestDto.getName())
//                        .product(userRequestDto.getProduct())
                        .mobilNumber(userRequestDto.getMobilNumber())
                        .build();
        final UserEntity userEntitySaved = userRepository.save(userEntity);
        return UserResponseDto
                .builder()
                .id(userEntitySaved.getId())
                .mobilNumber(userEntitySaved.getMobilNumber())
                .name(userEntitySaved.getName())
//                .product(userEntitySaved.getProduct())
                .build();
    }

    @Override
    public UserResponseDto getUserById(Long id) {
        final UserEntity user = userRepository.findById(id).orElseThrow();

        return UserResponseDto
                .builder()
                .id(user.getId())
                .name(user.getName())
                .product(user.getProduct())
                .mobilNumber(user.getMobilNumber())
                .build();
    }

    @Override
    public UserResponseDto updateUser(Long id, UserRequestDto userRequestDto) {
        UserEntity user =
                UserEntity
                        .builder()
                        .id(id)
                        .mobilNumber(userRequestDto.getMobilNumber())
                        .name(userRequestDto.getName())
//                        .product(userRequestDto.getProduct())
                        .build();
       final UserEntity userEntitySaved = userRepository.save(user);
       return UserResponseDto
               .builder()
               .id(userEntitySaved.getId())
//               .product(userEntitySaved.getProduct())
               .mobilNumber(userEntitySaved.getMobilNumber())
               .name(userEntitySaved.getName())
               .build();
    }

    @Override
    public void deleteUserById(Long id) {
    userRepository.deleteById(id);
    }

    @Override
    public Page<UserResponseDto> userList(@PageableDefault(size = 7,page = 1) Pageable page) {
        return userRepository.findAll(page)
                .map(userEntity -> UserResponseDto
                        .builder()
                        .id(userEntity.getId())
                        .name(userEntity.getName())
//                        .product(userEntity.getProduct())
                        .mobilNumber(userEntity.getMobilNumber())
                        .build());
    }
}
