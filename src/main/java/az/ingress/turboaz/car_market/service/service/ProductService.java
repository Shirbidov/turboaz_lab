package az.ingress.turboaz.car_market.service.service;

import az.ingress.turboaz.car_market.domain.ProductEntity;
import az.ingress.turboaz.car_market.dto.request.ProductRequestDto;
import az.ingress.turboaz.car_market.dto.response.ProductResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductService {


    ProductResponseDto createProduct(ProductRequestDto requestDto);

    ProductResponseDto getProductById(Long id);

    ProductResponseDto updateProduct(Long id, ProductRequestDto productRequestDto);

    void deleteProductById(Long id);

    Page<ProductResponseDto> productList(Pageable page);




    //todo delete all products


}
