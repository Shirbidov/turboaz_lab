package az.ingress.turboaz.car_market.service.Impl;

import az.ingress.turboaz.car_market.domain.ManufacturerEntity;
import az.ingress.turboaz.car_market.domain.ModelEntity;
import az.ingress.turboaz.car_market.domain.ProductEntity;
import az.ingress.turboaz.car_market.dto.request.ManufactureRequestDto;
import az.ingress.turboaz.car_market.dto.request.ProductRequestDto;
import az.ingress.turboaz.car_market.dto.response.ManufactureResponseDto;
import az.ingress.turboaz.car_market.dto.response.ProductResponseDto;
import az.ingress.turboaz.car_market.repository.ManufactureRepository;
import az.ingress.turboaz.car_market.repository.ProductRepository;
import az.ingress.turboaz.car_market.service.service.ManufactureService;
import az.ingress.turboaz.car_market.service.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ManufactureServiceImpl implements ManufactureService {

    private final ManufactureRepository manufactureRepository;


    @Override
    public ManufactureResponseDto createManufacture(ManufactureRequestDto manufactureRequestDto) {
//todo add here if required value is null to give exception about all field

        ManufacturerEntity manufacturerEntity =
                ManufacturerEntity
                        .builder()
//                        .model(manufactureRequestDto.getModels())
                        .name(manufactureRequestDto.getName())
                        .build();
        final ManufacturerEntity manufacturerSave = manufactureRepository.save(manufacturerEntity);
        return ManufactureResponseDto
                .builder()
//                .models(manufacturerSave.getModel())
                .name(manufacturerSave.getName())
                .build();
    }


    @Override
    public ManufactureResponseDto getManufactureById(Long id) {
        final ManufacturerEntity manufacturerEntity = manufactureRepository.findById(id)
                .orElseThrow(); // todo add here exception not found and id doesn not get letters or characters
//     final List<ModelEntity> modelEntity = manufacturerEntity.getModel();
        return ManufactureResponseDto
                //todo change builder with mapper
                .builder()
                .name(manufacturerEntity.getName())
                .models(manufacturerEntity.getModel())
                .build();
    }

    @Override
    public ManufactureResponseDto updateManufacture(Long id, ManufactureRequestDto manufactureRequestDto) {

        ManufacturerEntity manufacturerEntity =
                ManufacturerEntity
                        .builder()
                        .id(id)
                        .name(manufactureRequestDto.getName())
//                        .model(manufactureRequestDto.getModels())
                        .build();
        final ManufacturerEntity manufacturerEntitySaved = manufactureRepository.save(manufacturerEntity);
        return ManufactureResponseDto
                .builder()
                .name(manufacturerEntitySaved.getName())
//                .models(manufacturerEntitySaved.getModel())
                .build();
    }

    @Override
    public void deleteManufactureById(Long id) {
        manufactureRepository.deleteById(id);
    }

    @Override
    public Page<ManufactureResponseDto> manufactureList(@PageableDefault(size = 3, page = 1) Pageable pageable) {
        return manufactureRepository.findAll(pageable)
                .map(manufacturerEntity -> ManufactureResponseDto
                        .builder()
                        .name(manufacturerEntity.getName())
//                        .models(manufacturerEntity.getModel())
                        .build()
                );
    }

    @Override
    public ManufactureResponseDto searchByManufactory(String name) {
        final ManufacturerEntity manufacturerEntity =  manufactureRepository.findByName(name);
        return ManufactureResponseDto.builder()
                .name(manufacturerEntity.getName())
                .models(manufacturerEntity.getModel())
                .build();
    }


}
