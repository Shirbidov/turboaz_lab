package az.ingress.turboaz.car_market.service.Impl;

import az.ingress.turboaz.car_market.domain.ProductEntity;
import az.ingress.turboaz.car_market.dto.request.ProductRequestDto;
import az.ingress.turboaz.car_market.dto.response.ProductResponseDto;
import az.ingress.turboaz.car_market.repository.ProductRepository;
import az.ingress.turboaz.car_market.service.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;


    @Override
    public ProductResponseDto createProduct(ProductRequestDto requestDto) {
//todo add here if required value is null to give exception about all field

        ProductEntity productEntity =
                ProductEntity
                        .builder()
//                        .productAnalytics(requestDto.getProductAnalytics())
                        .advertisementId(requestDto.getAdvertisementId())
                        .productDetailText(requestDto.getProductDetailText())
                        .price(requestDto.getPrice())
                        .priceСurrency(requestDto.getPriceСurrency())
//                        .user(requestDto.getUser())
                        .name(requestDto.getName())
                        .build();
        final ProductEntity productSaved = productRepository.save(productEntity);
        return ProductResponseDto
                .builder()
                .id(productSaved.getId())
//                .productAnalytics(productSaved.getProductAnalytics())
                .advertisementId(productSaved.getAdvertisementId())
                .productDetailText(productSaved.getProductDetailText())
                .price(productSaved.getPrice())
                .priceСurrency(productSaved.getPriceСurrency())
                .name(productSaved.getName())
//                .user(productSaved.getUser())
                .build();

    }

    @Override
    public ProductResponseDto getProductById(Long id) {
        final ProductEntity product = productRepository.findById(id)
                .orElseThrow(); // todo add here exception not found and id doesn not get letters or characters
        return ProductResponseDto
                //todo change builder with mapper
                .builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .productDetailText(product.getProductDetailText())
//                .productAnalytics(product.getProductAnalytics())
                .advertisementId(product.getAdvertisementId())
                .priceСurrency(product.getPriceСurrency())
//                .user(product.getUser())
                .productDetailEntity(product.getProductDetailEntity())
                .build();
    }

    @Override
    public ProductResponseDto updateProduct(Long id, ProductRequestDto productRequestDto) {

        ProductEntity productEntity =
                ProductEntity
                        .builder()
                        .id(id)
//                        .productAnalytics(productRequestDto.getProductAnalytics())
                        .advertisementId(productRequestDto.getAdvertisementId())
                        .productDetailText(productRequestDto.getProductDetailText())
                        .price(productRequestDto.getPrice())
                        .priceСurrency(productRequestDto.getPriceСurrency())
//                        .user(productRequestDto.getUser())
                        .name(productRequestDto.getName())
                        .build();
        final ProductEntity productSaved = productRepository.save(productEntity);
        return ProductResponseDto
                .builder()
                .id(productSaved.getId())
//                .productAnalytics(productSaved.getProductAnalytics())
                .advertisementId(productSaved.getAdvertisementId())
                .productDetailText(productSaved.getProductDetailText())
                .price(productSaved.getPrice())
                .priceСurrency(productSaved.getPriceСurrency())
                .name(productSaved.getName())
//                .user(productSaved.getUser())
                .build();
    }

    @Override
    public void deleteProductById(Long id) {
        productRepository.deleteById(id);
    }

    @Override
    public Page<ProductResponseDto> productList(@PageableDefault(size = 7, page = 1) Pageable pageable) {
        return productRepository.findAll(pageable)
                .map(productEntity -> ProductResponseDto
                        .builder()
                        .id(productEntity.getId())
//                        .user(productEntity.getUser())
                        .priceСurrency(productEntity.getPriceСurrency())
                        .advertisementId(productEntity.getAdvertisementId())
                        .productDetailText(productEntity.getProductDetailText())
                        .price(productEntity.getPrice())
                        .name(productEntity.getName())
//                        .productAnalytics(productEntity.getProductAnalytics())
                        .build()
                );
    }
}
