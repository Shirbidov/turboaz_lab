package az.ingress.turboaz.car_market.service.service;

import az.ingress.turboaz.car_market.dto.request.ProductAnalyticsRequestDto;
import az.ingress.turboaz.car_market.dto.request.ProductDetailRequestDto;
import az.ingress.turboaz.car_market.dto.response.ProductAnalyticsResponseDto;
import az.ingress.turboaz.car_market.dto.response.ProductDetailResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductAnalyticsService {


    ProductAnalyticsResponseDto createProductAnalytics(ProductAnalyticsRequestDto productAnalyticsRequestDto);

    ProductAnalyticsResponseDto getProductAnalyticsById(Long id);

    ProductAnalyticsResponseDto updateProductAnalytics(Long id, ProductAnalyticsRequestDto productAnalyticsRequestDto);

    void deleteProductAnalyticsById(Long id);

    Page<ProductAnalyticsResponseDto> productAnalyticsList(Pageable page);




    //todo delete all productAnalytics


}
