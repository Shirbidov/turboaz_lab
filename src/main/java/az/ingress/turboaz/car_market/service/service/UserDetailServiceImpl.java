package az.ingress.turboaz.car_market.service.service;

import az.ingress.turboaz.car_market.repository.UserRoleRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class UserDetailServiceImpl implements UserDetailsService {

    private final UserRoleRepository userRoleRepository;
    @Override
    public UserDetails loadUserByUsername(String username) {return userRoleRepository.findByUsername(username)
                .orElseThrow(()-> new UsernameNotFoundException("user not found" + username));
    }
}
