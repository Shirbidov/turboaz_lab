package az.ingress.turboaz.car_market.service.Impl;

import az.ingress.turboaz.car_market.domain.ProductAnalyticsEntity;
import az.ingress.turboaz.car_market.domain.ProductDetailEntity;
import az.ingress.turboaz.car_market.dto.request.ProductAnalyticsRequestDto;
import az.ingress.turboaz.car_market.dto.request.ProductDetailRequestDto;
import az.ingress.turboaz.car_market.dto.response.ProductAnalyticsResponseDto;
import az.ingress.turboaz.car_market.dto.response.ProductDetailResponseDto;
import az.ingress.turboaz.car_market.repository.ProductAnalyticsRepository;
import az.ingress.turboaz.car_market.repository.ProductDetailRepository;
import az.ingress.turboaz.car_market.service.service.ProductAnalyticsService;
import az.ingress.turboaz.car_market.service.service.ProductDetailService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProductAnalyticsServiceImpl implements ProductAnalyticsService {

    private final ProductAnalyticsRepository productAnalyticsRepository;


    @Override
    public ProductAnalyticsResponseDto createProductAnalytics(ProductAnalyticsRequestDto productAnalyticsRequestDto) {
//todo add here if required value is null to give exception about all field

        ProductAnalyticsEntity productAnalyticsEntity =
                ProductAnalyticsEntity
                        .builder()
//                        .product(productAnalyticsRequestDto.getProduct())
                        .lastUpdateDate(productAnalyticsRequestDto.getLastUpdateDate())
                        .viewCount(productAnalyticsRequestDto.getViewCount())
                        .build();
        final ProductAnalyticsEntity productDetailEntitySaved = productAnalyticsRepository.save(productAnalyticsEntity);
        return ProductAnalyticsResponseDto
                .builder()
                .id(productDetailEntitySaved.getId())
                .lastUpdateDate(productDetailEntitySaved.getLastUpdateDate())
//                .product(productDetailEntitySaved.getProduct())
                .viewCount(productAnalyticsRequestDto.getViewCount())
                .build();

    }


    @Override

    public ProductAnalyticsResponseDto getProductAnalyticsById(Long id) {
        final ProductAnalyticsEntity productAnalytics = productAnalyticsRepository.findById(id)
                .orElseThrow(); // todo add here exception not found and id doesn not get letters or characters
        return ProductAnalyticsResponseDto
                //todo change builder with mapper
                .builder()
                .id(productAnalytics.getId())
                .viewCount(productAnalytics.getViewCount())
//                .product(productAnalytics.getProduct())
                .lastUpdateDate(productAnalytics.getLastUpdateDate())
                .build();
    }

    @Override
    public ProductAnalyticsResponseDto updateProductAnalytics(Long id, ProductAnalyticsRequestDto productAnalyticsRequestDto) {
        ProductAnalyticsEntity productAnalyticsEntity =
                ProductAnalyticsEntity
                        .builder()
                        .id(id)
//                        .product(productAnalyticsRequestDto.getProduct())
                        .viewCount(productAnalyticsRequestDto.getViewCount())
                        .lastUpdateDate(productAnalyticsRequestDto.getLastUpdateDate())
                        .build();
        final ProductAnalyticsEntity productAnalyticsEntitySaved = productAnalyticsRepository.save(productAnalyticsEntity);
        return ProductAnalyticsResponseDto
                .builder()
                .id(productAnalyticsEntitySaved.getId())
                .lastUpdateDate(productAnalyticsEntitySaved.getLastUpdateDate())
                .product(productAnalyticsRequestDto.getProduct())
                .viewCount(productAnalyticsRequestDto.getViewCount())
                .build();
    }

    @Override
    public void deleteProductAnalyticsById(Long id) {
        productAnalyticsRepository.deleteById(id);
    }

    @Override
    public Page<ProductAnalyticsResponseDto> productAnalyticsList(@PageableDefault(size = 7, page = 1) Pageable pageable) {
        return productAnalyticsRepository.findAll(pageable)
                .map(productAnalyticsEntity -> ProductAnalyticsResponseDto
                        .builder()
                        .id(productAnalyticsEntity.getId())
//                        .product(productAnalyticsEntity.getProduct())
                        .viewCount(productAnalyticsEntity.getViewCount())
                        .lastUpdateDate(productAnalyticsEntity.getLastUpdateDate())
                        .build()
                );
    }
}
