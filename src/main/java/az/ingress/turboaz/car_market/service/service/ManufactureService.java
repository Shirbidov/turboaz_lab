package az.ingress.turboaz.car_market.service.service;

import az.ingress.turboaz.car_market.domain.ManufacturerEntity;
import az.ingress.turboaz.car_market.dto.request.ManufactureRequestDto;
import az.ingress.turboaz.car_market.dto.request.ProductRequestDto;
import az.ingress.turboaz.car_market.dto.response.ManufactureResponseDto;
import az.ingress.turboaz.car_market.dto.response.ProductResponseDto;
import az.ingress.turboaz.car_market.repository.ManufactureRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ManufactureService {


    ManufactureResponseDto createManufacture(ManufactureRequestDto manufactureRequestDto);

    ManufactureResponseDto getManufactureById(Long id);

    ManufactureResponseDto updateManufacture(Long id, ManufactureRequestDto manufactureRequestDto);

    void deleteManufactureById(Long id);

    Page<ManufactureResponseDto> manufactureList(Pageable page);

    ManufactureResponseDto searchByManufactory(String name);




    //todo delete all products


}
