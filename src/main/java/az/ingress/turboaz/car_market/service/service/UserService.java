package az.ingress.turboaz.car_market.service.service;

import az.ingress.turboaz.car_market.dto.request.UserRequestDto;
import az.ingress.turboaz.car_market.dto.response.UserResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface UserService {


    UserResponseDto createUser(UserRequestDto userRequestDto);

    UserResponseDto getUserById(Long id);

    UserResponseDto updateUser(Long id, UserRequestDto userRequestDto);

    void deleteUserById(Long id);

    Page<UserResponseDto> userList(Pageable page);




    //todo delete all users


}
