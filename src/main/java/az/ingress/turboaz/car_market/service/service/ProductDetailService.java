package az.ingress.turboaz.car_market.service.service;

import az.ingress.turboaz.car_market.dto.request.ProductDetailRequestDto;
import az.ingress.turboaz.car_market.dto.request.ProductRequestDto;
import az.ingress.turboaz.car_market.dto.response.ProductDetailResponseDto;
import az.ingress.turboaz.car_market.dto.response.ProductResponseDto;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface ProductDetailService {


    ProductDetailResponseDto createProductDetail(ProductDetailRequestDto productDetailRequestDto);

    ProductDetailResponseDto getProductDetailById(Long id);

    ProductDetailResponseDto updateProductDetail(Long id, ProductDetailRequestDto productDetailRequestDto);

    void deleteProductDetailById(Long id);

    Page<ProductDetailResponseDto> productDetailList(Pageable page);




    //todo delete all productDetail


}
