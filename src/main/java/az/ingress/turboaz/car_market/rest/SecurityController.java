package az.ingress.turboaz.car_market.rest;

import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/security")
public class SecurityController {

    @GetMapping("/public")
    public String sayHello(){
        return "Hello from public";
    }

    @GetMapping("/authenticated") // we need for log in
    public String sayHelloAuthenticated(Authentication authentication){
        return "Hello + " + authentication.getName();
    }

    @GetMapping("/role-user") // we need for log in by user
    public String sayHelloRoleUser(){
        return "Hello from role-user";
    }

    @GetMapping("/role-admin") // we need for log in by admin
    public String sayHelloRoleAdmin(){
        return "Hello from role-admin";
    }
}
