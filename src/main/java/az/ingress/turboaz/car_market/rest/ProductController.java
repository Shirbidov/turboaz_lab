package az.ingress.turboaz.car_market.rest;


import az.ingress.turboaz.car_market.domain.ProductEntity;
import az.ingress.turboaz.car_market.dto.request.ProductRequestDto;
import az.ingress.turboaz.car_market.dto.response.ProductResponseDto;
import az.ingress.turboaz.car_market.service.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/product")
@RequiredArgsConstructor
public class ProductController {

    private final ProductService productService;

    @GetMapping("/{id}")
    public ProductResponseDto getProductById(@PathVariable Long id) {
        return productService.getProductById(id);
    }

    //todo try create product with all params
    @PostMapping()
    public ProductResponseDto createProduct(@RequestBody @Validated ProductRequestDto productRequestDto) {
        return productService.createProduct(productRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteProductById(@PathVariable Long id){
        productService.deleteProductById(id);
    }

    @PutMapping("/{id}")
    public ProductResponseDto update
            (@PathVariable Long id,
             @RequestBody ProductRequestDto productRequestDto){
        return productService.updateProduct(id, productRequestDto);
    }

    @GetMapping
    public Page<ProductResponseDto> getProductList(Pageable pageable){
        return productService.productList(pageable);
    }




}
