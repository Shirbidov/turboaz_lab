package az.ingress.turboaz.car_market.rest;


import az.ingress.turboaz.car_market.dto.request.ManufactureRequestDto;
import az.ingress.turboaz.car_market.dto.request.ModelRequestDto;
import az.ingress.turboaz.car_market.dto.response.ManufactureResponseDto;
import az.ingress.turboaz.car_market.dto.response.ModelResponseDto;
import az.ingress.turboaz.car_market.service.service.ManufactureService;
import az.ingress.turboaz.car_market.service.service.ModelService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/model")
@RequiredArgsConstructor
public class ModelController {

    private final ModelService modelService;

    @GetMapping("/{id}")
    public ModelResponseDto getModelById(@PathVariable Long id) {
        return modelService.getModelById(id);
    }

    //todo try create model with all params
    @PostMapping()
    public ModelResponseDto createModel(@RequestBody @Validated ModelRequestDto modelRequestDto) {
        return modelService.createModel(modelRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteModelById(@PathVariable Long id){
        modelService.deleteModelById(id);
    }

    @PutMapping("/{id}")
    public ModelResponseDto update
            (@PathVariable Long id,
             @RequestBody ModelRequestDto modelRequestDto){
        return modelService.updateModel(id, modelRequestDto);
    }

    @GetMapping
    public Page<ModelResponseDto> getModelList(Pageable pageable){
        return modelService.modelList(pageable);
    }




}
