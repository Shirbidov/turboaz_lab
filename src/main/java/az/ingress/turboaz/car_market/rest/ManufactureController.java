package az.ingress.turboaz.car_market.rest;


import az.ingress.turboaz.car_market.dto.request.ManufactureRequestDto;
import az.ingress.turboaz.car_market.dto.request.ProductRequestDto;
import az.ingress.turboaz.car_market.dto.response.ManufactureResponseDto;
import az.ingress.turboaz.car_market.dto.response.ProductResponseDto;
import az.ingress.turboaz.car_market.service.service.ManufactureService;
import az.ingress.turboaz.car_market.service.service.ProductService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manufacture")
@RequiredArgsConstructor
public class ManufactureController {

    private final ManufactureService manufactureService;

    @GetMapping("/{id}")
    public ManufactureResponseDto getManufactureById(@PathVariable Long id) {
        return manufactureService.getManufactureById(id);
    }

    //todo try create product with all params
    @PostMapping()
    public ManufactureResponseDto createManufacture(@RequestBody @Validated ManufactureRequestDto manufactureRequestDto) {
        return manufactureService.createManufacture(manufactureRequestDto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteManufactureById(@PathVariable Long id){
        manufactureService.deleteManufactureById(id);
    }

    @PutMapping("/{id}")
    public ManufactureResponseDto update
            (@PathVariable Long id,
             @RequestBody ManufactureRequestDto manufactureRequestDto){
        return manufactureService.updateManufacture(id, manufactureRequestDto);
    }

    @GetMapping
    public Page<ManufactureResponseDto> getManufactureList(Pageable pageable){
        return manufactureService.manufactureList(pageable);
    }


    @GetMapping("/search/{name}")
    public ManufactureResponseDto search(@PathVariable String name){
        return manufactureService.searchByManufactory(name);
    }




}
