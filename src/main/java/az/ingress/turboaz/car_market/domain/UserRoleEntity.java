package az.ingress.turboaz.car_market.domain;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.boot.autoconfigure.web.WebProperties;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.List;

@Getter
@Builder
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class UserRoleEntity implements UserDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany
    public List<UserRoleAithority> authorities;

    private boolean accountNonLocked;

    private String password;

    private String username;


    private boolean accountNonExpired;


    private boolean credentialsNonExpired;


    private boolean enabled;


}
