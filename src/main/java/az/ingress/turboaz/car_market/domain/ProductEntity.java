package az.ingress.turboaz.car_market.domain;

import jakarta.persistence.*;
import lombok.*;

import java.util.List;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
//@ToString
@Builder

public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
//    @ManyToOne(targetEntity = UserEntity.class)
//    @ToString.Exclude
//    private UserEntity user;
    private String name;
    private Double price;
    private String priceСurrency;
    private String productDetailText;
    private Long advertisementId;
    @OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private ProductDetailEntity productDetailEntity;
//    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
//    private ProductAnalyticsEntity productAnalytics;
}
