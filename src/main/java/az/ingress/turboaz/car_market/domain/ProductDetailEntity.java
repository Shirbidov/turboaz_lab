package az.ingress.turboaz.car_market.domain;

import jakarta.persistence.*    ;
import lombok.*;

import java.util.List;


@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@Builder
public class ProductDetailEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String city;
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private List<ManufacturerEntity> manufacturer;
    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
    private List<ModelEntity> model;
    private Long manufacturerDate;
    private String color;
    private String engine;
    private Long mileage;
    private String transmissionType;
    private String driveUnit;
    private Boolean isNew;
    private Long numberOfSeats;

}