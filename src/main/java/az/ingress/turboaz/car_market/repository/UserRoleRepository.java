package az.ingress.turboaz.car_market.repository;

import az.ingress.turboaz.car_market.domain.UserEntity;
import az.ingress.turboaz.car_market.domain.UserRoleEntity;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Optional;

public interface UserRoleRepository extends JpaRepository<UserRoleEntity, Long> {

    @EntityGraph(attributePaths = "authorities")
    Optional<UserRoleEntity> findByUsername(String username);
}
