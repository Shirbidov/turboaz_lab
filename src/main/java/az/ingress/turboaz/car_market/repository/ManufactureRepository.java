package az.ingress.turboaz.car_market.repository;

import az.ingress.turboaz.car_market.domain.ManufacturerEntity;
import az.ingress.turboaz.car_market.domain.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ManufactureRepository extends JpaRepository<ManufacturerEntity,Long> {

   ManufacturerEntity findByName(String name);




}
