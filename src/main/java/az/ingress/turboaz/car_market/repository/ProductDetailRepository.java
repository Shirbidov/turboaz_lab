package az.ingress.turboaz.car_market.repository;

import az.ingress.turboaz.car_market.domain.ProductDetailEntity;
import az.ingress.turboaz.car_market.domain.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDetailRepository extends JpaRepository<ProductDetailEntity,Long> {

}
