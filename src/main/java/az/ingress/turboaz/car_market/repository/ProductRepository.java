package az.ingress.turboaz.car_market.repository;

import az.ingress.turboaz.car_market.domain.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductRepository extends JpaRepository<ProductEntity,Long> {

}
