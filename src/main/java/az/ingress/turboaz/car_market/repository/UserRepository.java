package az.ingress.turboaz.car_market.repository;

import az.ingress.turboaz.car_market.domain.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<UserEntity, Long> {

}
