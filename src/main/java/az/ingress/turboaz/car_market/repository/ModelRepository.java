package az.ingress.turboaz.car_market.repository;

import az.ingress.turboaz.car_market.domain.ManufacturerEntity;
import az.ingress.turboaz.car_market.domain.ModelEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ModelRepository extends JpaRepository<ModelEntity,Long> {





}
