package az.ingress.turboaz.car_market.dto.response;

import az.ingress.turboaz.car_market.domain.ModelEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ModelResponseDto {
    private String name;
}
