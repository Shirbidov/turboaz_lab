package az.ingress.turboaz.car_market.dto.request;

import az.ingress.turboaz.car_market.domain.ManufacturerEntity;
import az.ingress.turboaz.car_market.domain.ModelEntity;
import az.ingress.turboaz.car_market.domain.ProductAnalyticsEntity;
import az.ingress.turboaz.car_market.domain.UserEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.ManyToMany;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProductDetailRequestDto {
    private String city;
    private List<ManufacturerEntity> manufacturer;
    private List<ModelEntity> model;
    private Long manufacturerDate;
    private String color;
    private String engine;
    private Long mileage;
    private String transmissionType;
    private String driveUnit;
    private Boolean isNew;
    private Long numberOfSeats;
}


