package az.ingress.turboaz.car_market.dto.request;

import az.ingress.turboaz.car_market.domain.ProductEntity;
import jakarta.persistence.*;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserRequestDto {
    private String name;
    private String mobilNumber;
    private List<ProductEntity> product;
}
