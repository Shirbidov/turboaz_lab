package az.ingress.turboaz.car_market.dto.request;

import az.ingress.turboaz.car_market.domain.ManufacturerEntity;
import az.ingress.turboaz.car_market.domain.ModelEntity;
import az.ingress.turboaz.car_market.domain.ProductEntity;
import jakarta.persistence.CascadeType;
import jakarta.persistence.FetchType;
import jakarta.persistence.OneToMany;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class ProductAnalyticsRequestDto {
    private List<ProductEntity> product;
    private Date lastUpdateDate;
    private Long viewCount;
}


