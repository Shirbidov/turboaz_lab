package az.ingress.turboaz.car_market.dto.request;

import az.ingress.turboaz.car_market.domain.ModelEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;
@Data
@Builder
public class ManufactureRequestDto {
    private Long id;
    private String name;
    private List<ModelEntity> models;
}
