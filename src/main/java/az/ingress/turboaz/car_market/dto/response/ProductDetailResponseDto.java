package az.ingress.turboaz.car_market.dto.response;

import az.ingress.turboaz.car_market.domain.ManufacturerEntity;
import az.ingress.turboaz.car_market.domain.ModelEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProductDetailResponseDto {
    private Long id;
    private String city;
    private List<ManufacturerEntity> manufacturer;
    private List<ModelEntity> model;
    private Long manufacturerDate;
    private String color;
    private String engine;
    private Long mileage;
    private String transmissionType;
    private String driveUnit;
    private Boolean isNew;
    private Long numberOfSeats;
}


