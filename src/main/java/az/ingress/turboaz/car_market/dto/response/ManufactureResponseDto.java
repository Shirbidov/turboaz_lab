package az.ingress.turboaz.car_market.dto.response;

import az.ingress.turboaz.car_market.domain.ModelEntity;
import lombok.Builder;
import lombok.Data;
import org.springframework.data.domain.Page;

import java.util.List;

@Data
@Builder
public class ManufactureResponseDto {

    private String name;
    private List<ModelEntity> models;

}
