package az.ingress.turboaz.car_market.dto.response;

import az.ingress.turboaz.car_market.domain.ProductEntity;
import lombok.Builder;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
@Builder
public class ProductAnalyticsResponseDto {
    private Long id;
    private List<ProductEntity> product;
    private Date lastUpdateDate;
    private Long viewCount;
}
