package az.ingress.turboaz.car_market.dto.response;

import az.ingress.turboaz.car_market.domain.ProductEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class UserResponseDto {
    private Long id;
    private String name;
    private String mobilNumber;
    private List<ProductEntity> product;
}
