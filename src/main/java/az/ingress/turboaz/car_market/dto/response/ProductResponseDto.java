package az.ingress.turboaz.car_market.dto.response;

import az.ingress.turboaz.car_market.domain.ProductDetailEntity;
import az.ingress.turboaz.car_market.domain.UserEntity;
import az.ingress.turboaz.car_market.domain.ProductAnalyticsEntity;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class ProductResponseDto {
    private Long id;
//    private UserEntity user;
    private String name;
    private Double price;
    private String priceСurrency;
    private String productDetailText;
    private Long advertisementId;
    private List<ProductAnalyticsEntity> productAnalytics;
    private ProductDetailEntity productDetailEntity;
}
